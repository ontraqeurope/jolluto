FROM gcr.io/ontraq-test/otphp:7.3-apache

COPY www/site /var/www/site

RUN chown -R www-data:www-data /var/www/site

COPY config/apache2/apache2.conf /etc/apache2/apache2.conf

COPY config/apache2/000-default.conf /etc/apache2/sites-available/000-default.conf

COPY config/php/php.ini /usr/local/etc/php/php.ini
